package myprojects.automation.assignment5;

import myprojects.automation.assignment5.Pages.MainPage;
import myprojects.automation.assignment5.Pages.ProductCard;
import myprojects.automation.assignment5.Pages.ProductCart;
import myprojects.automation.assignment5.Pages.ProductOrder;
import myprojects.automation.assignment5.model.CustomerData;
import myprojects.automation.assignment5.model.ProductData;
import myprojects.automation.assignment5.utils.DataConverter;
import myprojects.automation.assignment5.utils.logging.CustomReporter;
import org.openqa.selenium.By;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

/**
 * Contains main script actions that may be used in scripts.
 */
public class GeneralActions {
    private EventFiringWebDriver driver;
    private WebDriverWait wait;

    private MainPage mainPage = null;
    private ProductCard productCard = null;
    private ProductCart productCart = null;
    private ProductOrder productOrder = null;

    private By inputProduct = By.name("s");
    private By goOrderButton = By.cssSelector("a.btn.btn-primary");
    private By productQtyAll = By.cssSelector("div.product-quantities > span");
    private By productPrice = By.cssSelector("span[content]");
    private By productDetails = By.cssSelector("a[href='#product-details']");

    private String name;
    private int qtyWanted;
    private Float price;
    private int qtyAll;

    private String nameInCart;
    private Float subtotal;
    private Float priceInCart;
    private Float priceInCartBold;
    private int qtyInCart;
    private int qtySubtotal;
    private Float total;
    private Float totalCalc;

    private String carsTitleInOrder;
    private int qtyInOrder;
    private String nameInOrder;
    private Float priceInOrder;

    int productQtyAllActual;

    private CharSequence titleInOrderExpected;

    public GeneralActions(EventFiringWebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
    }

    public void checkSiteVersion(Boolean actualVersion) {
        CustomReporter.logAction("Open main page and validate website version");
        mainPage = new MainPage(driver);

        mainPage.openMainPage();
        waitForContentLoad(inputProduct);

        Boolean expectedVersion = mainPage.isMobileSiteVersion();
        Assert.assertEquals(actualVersion, expectedVersion, "Validate website version faild");
    }

    public void openRandomProduct() {
        // TODO implement logic to open random product before purchase
        CustomReporter.logAction("Open random product before purchase");
        mainPage = new MainPage(driver);

        mainPage.clickAllProducts();
        waitForContentLoad(inputProduct);

        mainPage.openProductCard();
        waitForContentLoad(inputProduct);
    }

    /**
     * Extracts product information from opened product details page.
     *
     * @return
     */
    public ProductData getOpenedProductInfo() {
        CustomReporter.logAction("Get information about currently opened product");
        // TODO extract data from opened page
        productCard = new ProductCard(driver);
        waitForContentLoad(productDetails);

        productCard.clickProductDetails();
        waitForContentLoad(productQtyAll);

        name = productCard.getProductName();
        qtyWanted = productCard.getQtyWanted();

        qtyAll = productCard.getProductQtyAll();
        waitForContentLoad(productPrice);

        price = productCard.getProductPrice();

        return new ProductData(name, qtyWanted, price, qtyAll);
    }

    public void addProductToCart() {
        CustomReporter.logAction("Add product to cart");
        productCard.addProductToCart();
        waitForContentLoad(goOrderButton);
        productCard.goToOrder();
    }

    public void waitForContentLoad(By locator) {
        // implement generic method to wait until page content is loaded
        wait.until(ExpectedConditions.elementToBeClickable(locator));
    }

    public void validateProductInfo(ProductData productData) {
        CustomReporter.logAction("Validate product info before purchase");
        productCart = new ProductCart(driver);

        nameInCart = productCart.getName();
        priceInCart = productCart.getPrice();
        priceInCartBold = productCart.getPriceBold();
        qtyInCart = productCart.getQty();
        qtySubtotal = productCart.getQtySubtotal();
        total = productCart.getTotal();
        subtotal = productCart.getSubTotal();
        totalCalc = priceInCartBold * qtySubtotal;

        Assert.assertEquals(nameInCart.toLowerCase(), productData.getName().toLowerCase(), "Unexpected name");
        Assert.assertEquals(priceInCart, productData.getPrice(), "Unexpected price");
        Assert.assertEquals(priceInCartBold, productData.getPrice(), "Unexpected price (bold)");
        Assert.assertEquals(qtyInCart, productData.getQty(), "Unexpected quantity");
        Assert.assertEquals(qtySubtotal, productData.getQty(), "Unexpected subtotal quantity");
        Assert.assertEquals(subtotal, productData.getPrice(), "Unexpected subtotal");
        Assert.assertEquals(total, productData.getPrice(), "Unexpected total");
        Assert.assertEquals(total, totalCalc, "Unexpected total");

        productCart.clickOrderButton();
    }

    public void fillCustomerData(CustomerData customerData) {
        CustomReporter.logAction("Fill customer data");
        productOrder = new ProductOrder(driver);

        //waitForContentLoad();
        productOrder.setFirstname(customerData.getFirstname());
        productOrder.setLastname(customerData.getLastname());
        productOrder.setEmail(customerData.getEmail());
        productOrder.clickContinueButton();

        productOrder.setAddress(customerData.getAddress1());
        productOrder.setPostcode(customerData.getPostcode());
        productOrder.setCity(customerData.getCity());
        productOrder.clickConfirmButton();

        productOrder.clickConfirmDeliveryOptionButton();

        productOrder.setPaymentOption2();
        productOrder.setConditions_to_approve();
        productOrder.clickConfirmPaymentOptionButton();
    }

    public void validateOrderSummary(ProductData productData) {
        CustomReporter.logAction("Validate order summary");
        productOrder = new ProductOrder(driver);

        titleInOrderExpected = "ВАШ ЗАКАЗ ПОДТВЕРЖДЁН";

        carsTitleInOrder = productOrder.getCardTitle();
        nameInOrder = productOrder.getProductName().toUpperCase();
        priceInOrder = DataConverter.parsePriceValue(productOrder.getProductPrice());
        qtyInOrder = Integer.valueOf(productOrder.getProductQty());

        Assert.assertTrue(carsTitleInOrder.contains(titleInOrderExpected), "Unexpected total");
        Assert.assertTrue(nameInOrder.contains(productData.getName()), "Unexpected name");
        Assert.assertEquals(priceInOrder, productData.getPrice(), "Unexpected price");
        Assert.assertEquals(qtyInOrder, productData.getQty(), "Unexpected quantity");
    }

    public void checkUpdateInStockValue(ProductData productData) {
        CustomReporter.logAction("Check update in stock value");

        mainPage = new MainPage(driver);
        productCard = new ProductCard(driver);

        mainPage.setInputProduct(productData.getName());
        mainPage.clickSearchButton();
        waitForContentLoad(inputProduct);

        mainPage.openProductCard();
        productCard.clickProductDetails();
        waitForContentLoad(productQtyAll);

        productQtyAllActual = productCard.getProductQtyAll();

        Assert.assertEquals(productQtyAllActual, (productData.getQtyAll() - productData.getQty()), "Quantity in stock is incorrect");
    }
}
